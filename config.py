BASE_DIR = '/opt/sauron/data/'

USE_FINGERPRINTS = False

MAX_GAP = 250  # in frames

WINDOW_SIZE = 500  # in frames

BASE_SCORE = 1

GAP_PENALTY = 1  # in [0, 1]

CHANGE_PENALTY = 1


LOCAL_ALIGNMENT = False  # whether to use negative match scoring (Smith-Waterman)

DOWNSCALE_SIZE = 100

DOWNSCALE_RATIO = 0.25
