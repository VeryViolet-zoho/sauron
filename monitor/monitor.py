import os
import glob
import numpy as np

import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler

from sequence.batch import BatchAnalyser


class CreationHandler(FileSystemEventHandler):
    SIGNAL_FILE = '.ready'

    def on_created(self, event):

        if event.is_directory or os.path.basename(event.src_path) != self.SIGNAL_FILE:
            return

        print('--> Ready to process folder found {}.'.format(os.path.dirname(event.src_path)))
        print('--> Processing...')
#        try:
        processor = BatchAnalyser()
        processor.analyze(os.path.dirname(event.src_path))
 #       except Exception as e:
#            print('--> Failed: {}'.format(e))


class Monitor:
    base_directory = None

    def run(self, basedir):
        self.base_directory = basedir

        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')

        logger = LoggingEventHandler()
        event_handler = CreationHandler()

        observer = Observer()
        observer.schedule(event_handler, basedir, recursive=True)
        observer.schedule(logger, basedir, recursive=True)
        observer.start()
        try:
            while True:
                time.sleep(10)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()
