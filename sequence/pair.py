import typing
from compare.image import ImageComparator
from video.reader import VideoReader
from video.writer import VideoWriter
import numpy as np
import cv2
from PIL import Image

from scoop import futures

from imagehash import average_hash, whash

import config

import time

import click


class Route:
    ALIGNED = 0
    CUTTING = 1
    INSERTION = 2
    EDITING = 3


ModificationStrings = {Route.CUTTING: 'cutting', Route.INSERTION: 'insertion', Route.EDITING: 'editing'}


class Modification:
    start = None
    finish = None
    modtype = ''

    def __init__(self, start, finish, modtype: str):
        self.start = start
        self.finish = finish
        self.modtype = modtype


class PairSequenceAnalyser:
    comparator = None

    _sync_points = (0, 0)
    _localsync = (0, 0)
    _writesync = (0, 0)
    _maxpoint = (0, 0)

    _resync_point = None
    _trace_point = None
    _offsets = None

    _frames = [[], []]
    _images = [[], []]

    _readers = [None, None]

    _result = []

    _comparisons = None
    _scoring = None

    _max_in_row = None
    _max_in_col = None

    _appended = False

    _addons = None

    _trace = None

    _endofvideo = False

    _addons = [np.array([-1, -1]), np.array([-1, 0]), np.array([0, -1])]
    _addon_align = np.array([-1, -1])
    _addon_cutting = np.array([-1, 0])
    _addon_insertion = np.array([0, -1])

    def __init__(self):
        self.comparator = ImageComparator()
        self._comparisons = np.zeros((config.WINDOW_SIZE * 2, config.WINDOW_SIZE * 2))
        self._scoring = np.zeros((config.WINDOW_SIZE * 2 + 1, config.WINDOW_SIZE * 2 + 1))
        self._max_in_col = np.zeros((config.WINDOW_SIZE * 2 + 1,))
        self._max_in_row = np.zeros((config.WINDOW_SIZE * 2 + 1,))
        # aligned, cutting, insertion
        # self._addons = [np.array([-1, -1]), np.array([-1, 0]), np.array([0, -1])]
        self._offsets = np.array([0, 0])
        # self._trace = np.zeros((config.WINDOW_SIZE * 2,))
        self._endofvideo = False

    def fill_frames(self, append=False):
        #        if self._frames is not None:
        #            del self._frames
        if not append:
            self._images = [[], []]
            self._frames = [[], []]

        for i in range(2):
            if append:
                self._readers[i].pos(self._sync_points[i] + int(config.WINDOW_SIZE))
            else:
                self._readers[i].pos(self._offsets[i])

        for _ in range(int(config.WINDOW_SIZE)):
            for i in range(2):
                fr = self._readers[i].next_frame()
#                r = float(config.DOWNSCALE_SIZE) / fr.shape[1]
#                dim = (int(config.DOWNSCALE_SIZE), int(fr.shape[0] * r))
                dim = (int(fr.shape[1] * config.DOWNSCALE_RATIO), int(fr.shape[0] * config.DOWNSCALE_RATIO))
                if config.DOWNSCALE_RATIO < 1.0:
                    fr = cv2.resize(fr, dim, interpolation=cv2.INTER_AREA)
#                self._images[i].append(fr)
                self._frames[i].append(average_hash(Image.fromarray(fr), 16))

    def fill_comparisons(self, append=False):

        if append:
            d = int(config.WINDOW_SIZE)
        else:
            d = 0
            self._comparisons.fill(0.0)

        for i in range(config.WINDOW_SIZE):
            for j in range(config.WINDOW_SIZE):
                cmp = self.comparator.compare_vprints(self._frames[0][d + i], self._frames[1][d + j])
                self._comparisons[d + i, d + j] = 1 if cmp else -1
                # self._comparisons[d + j, d + i] = 1 if cmp else -1

                if append:
                    cmpI = self.comparator.compare_vprints(self._frames[0][d + i], self._frames[1][j])
                    self._comparisons[i + d, j] = 1 if cmpI else -1
                    #                    self._comparisons[j, i + d] = 1 if cmpI else -1
                    cmpJ = self.comparator.compare_vprints(self._frames[0][i], self._frames[1][d + j])
                    self._comparisons[i, j + d] = 1 if cmpJ else -1
                    #                   self._comparisons[j + d, i] = 1 if cmpJ else -1

    def fix_score(self, p, q):
        i = p - 1
        j = q - 1
        align_score = self._scoring[p - 1, q - 1] + config.BASE_SCORE * self._comparisons[i, j]
        self._max_in_col[q] = max([self._scoring[p - 1, q], self._max_in_col[q]])
        self._max_in_row[p] = max([self._scoring[p, q - 1], self._max_in_row[p]])
        insert_score = self._max_in_col[q]  # - config.GAP_PENALTY
        cut_score = self._max_in_row[p]  # - config.GAP_PENALTY
        self._scoring[p, q] = max([align_score, insert_score, cut_score, 0])

    def fill_scorings(self, append=False):

        if append:
            for p in range(1, config.WINDOW_SIZE + 1):
                for q in range(config.WINDOW_SIZE + 1, 2 * config.WINDOW_SIZE + 1):
                    self.fix_score(p, q)
            for p in range(config.WINDOW_SIZE + 1, 2 * config.WINDOW_SIZE + 1):
                for q in range(1, config.WINDOW_SIZE + 1):
                    self.fix_score(p, q)
            for p in range(config.WINDOW_SIZE + 1, 2 * config.WINDOW_SIZE + 1):
                for q in range(config.WINDOW_SIZE + 1, 2 * config.WINDOW_SIZE + 1):
                    self.fix_score(p, q)
        else:
            self._scoring.fill(0.0)
            self._max_in_col.fill(-1.0)
            self._max_in_row.fill(-1.0)

            for p in range(1, config.WINDOW_SIZE + 1):
                for q in range(1, config.WINDOW_SIZE + 1):
                    self.fix_score(p, q)

    def write_pack(self):
        pass

    def trace_back(self):

        self._trace = []

        point = self._resync_point
        route = Route.ALIGNED

        while tuple(point) != (0, 0):
            new_points = [point + self._addons[ddd] for ddd in range(3)]
            directions = [self._scoring[tuple(p)] for p in new_points]

            route = directions.index(max(directions))

            if route == Route.ALIGNED and not self._comparisons[tuple(new_points[route])]:
                route = Route.EDITING

            self._trace.append({'mode': route, 'point': point})
            point = new_points[route]

        self._trace.append({'mode': route, 'point': point})

        self._trace = list(reversed(self._trace))

    def advance(self, delta, mode):
        new_offsets = self._offsets + delta
        if mode != Route.ALIGNED:
            print('--> Found {} at ({}, {})'.format(ModificationStrings[mode], self._offsets[1], new_offsets[1]))
            self._result.append(Modification(self._offsets[1], new_offsets[1], ModificationStrings[mode]))

    def advance_to_resync(self):
        self._offsets = self._offsets + self._resync_point

    def analyze_trace(self):

        #        if self._trace_point[0] > self._trace_point[1]:
        #            mode = Route.CUTTING
        #        elif self._trace_point[0] < self._trace_point[1]:
        #            mode = Route.INSERTION
        #        else:
        #            mode = Route.ALIGNED

        mode = self._trace[0]['mode']

        startpoint = np.array([0, 0])
        for i in range(len(self._trace)):
            if self._trace[i]['mode'] != mode:
                newstartpoint = self._trace[i]['point']
                self.advance(newstartpoint - startpoint, mode)
                startpoint = newstartpoint
                mode = self._trace[i]['mode']

        self.advance(self._resync_point, mode)

    def find_resync(self):
        maxpoint = tuple(np.unravel_index(self._scoring.argmax(), self._scoring.shape))

        new_points = [maxpoint + self._addons[ddd] for ddd in range(3)]
        directions = [self._scoring[tuple(p)] for p in new_points]
        route = directions.index(max(directions))

        if route == Route.ALIGNED:
            self._resync_point = np.array(maxpoint)
            return True
        else:
            return False

    def process(self, original: VideoReader, modified: VideoReader, restored: VideoWriter):

        self._endofvideo = False
        self._result = []

        self._sync_points = [0, 0]
        self._readers = [original, modified]

        #        self.advance_direct()

        # if self._endofvideo:
        #            return self._result

        self.fill_frames()

        while len(self._frames[0]) != 0:
            start = time.time()

            if self._offsets[0] == 15000:
                irw = 444

            self.fill_comparisons()

            self.fill_scorings()

            if not self.find_resync():
                self.fill_frames(append=True)
                self.fill_comparisons(append=True)
                self.fill_scorings(append=True)
                if not self.find_resync():
                    raise ValueError('failed to find a sync')

            self.trace_back()

            self.analyze_trace()

            self.advance_to_resync()

            self.fill_frames()

            finish = time.time()

            print('-({})-> Window processed in {} seconds'.format(self._offsets[1], finish - start))

        return self._result
