import os
from video.reader import VideoReader
from video.writer import VideoWriter

from .pair import PairSequenceAnalyser


class BatchAnalyser:
    _batchdir = None

    _original_reader = None
    _subjects = []

    def init_original(self):
        original_dir = os.path.join(self._batchdir, 'original')

        if not os.path.exists(original_dir):
            print('--> No original directory in {}.'.format(self._batchdir))
            return False

        origlist = os.listdir(original_dir)

        if len(origlist) == 0:
            print('--> No original files in {}.'.format(original_dir))
            return False

        original_file = os.path.join(original_dir, origlist[0])

        try:
            self._original_reader = VideoReader(original_file)
        except Exception as e:
            print('--> Failed to open {} with exception: {}'.format(original_file, e))
            return False

        return True

    def init_batch(self):
        self._subjects.clear()
        batchlist = os.listdir(self._batchdir)

        if len(batchlist) == 0:
            print('--> No files in {}.'.format(self._batchdir))
            return False

        restoreddir = os.path.join(self._batchdir, 'restored')

        if not os.path.exists(restoreddir):
            os.mkdir(restoreddir)

        for fname in batchlist:
            data_file = os.path.join(self._batchdir, fname)
            restored_file = os.path.join(restoreddir, fname)
            try:
                datareader = VideoReader(data_file)
                restoredwriter = VideoWriter(restored_file, fps=datareader.get_fps(),
                                             framesize=datareader.get_framesize())
            except Exception as e:
                print('--> Failed to open {} with exception: {}'.format(data_file, e))
                continue
            self._subjects.append({'file': data_file, 'reader': datareader, 'restored': restoredwriter})

        return len(self._subjects) != 0

    def analyze(self, batchdir):

        self._batchdir = batchdir

        if not self.init_original():
            return False

        if not self.init_batch():
            return False

        for subj in self._subjects:
            pair = PairSequenceAnalyser()
            self.save_result(pair.process(self._original_reader, subj['reader'], subj['restored']), subj['file'])

    def save_result(self, res, subjname):
        pass
