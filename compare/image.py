import typing
import numpy as np


class DirectComparison:
    MEANDIFF = 0
    CORRCOEFF = 1


class VPrintComparison:
    AVH = 0
    CLD = 1
    EHD = 2


class ImageComparator:
    _method_direct = DirectComparison.CORRCOEFF  # type: DirectComparison
    _method_vprint = VPrintComparison.AVH  # type: VPrintComparison

    def __init__(self):
        pass

    def compare_direct(self, one, two):
        if self._method_direct == DirectComparison.MEANDIFF:
            return 1.0 - np.sum(np.abs(one - two)) / (255.0 * one.size)
        else:
            return 1.0

    def hamming(self, a, b):
        return bin(a ^ b).count('1')

    def compare_vprints(self, oneh, twoh):
        return True if (oneh-twoh) < 3 else False
