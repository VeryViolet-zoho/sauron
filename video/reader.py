import cv2
import numpy as np
import os


class VideoReader:
    _filename = None
    _capture = None

    def __init__(self, filename):
        self._filename = filename
        self._capture = cv2.VideoCapture(self._filename)

    def __del__(self):
        self._capture.release()

    def restart(self):
        self._capture.set(1, 0)

    def next_frame(self):
        if not self._capture.isOpened():
            return None
        else:
            ret, frame = self._capture.read()
            return frame

    def pos(self, no):
        self._capture.set(1, no)

    def frame(self, no):
        self.pos(no)
        ret, frame = self._capture.read()
        return frame

    def get_fps(self):
        return self._capture.get(cv2.CAP_PROP_FPS)

    def get_framesize(self):
        return (int(self._capture.get(cv2.CAP_PROP_FRAME_WIDTH)), int(self._capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
