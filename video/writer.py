import cv2
import numpy as np
import os


class VideoWriter:
    _filename = None
    _writer = None

    def __init__(self, filename, fps, framesize):
        self._filename = filename
        fourcc = cv2.VideoWriter_fourcc('X', '2', '6', '4')
        self._writer = cv2.VideoWriter(self._filename, fourcc=fourcc, fps=fps, frameSize=framesize)

    def __del__(self):
        self._writer.release()

    def write_frame(self, im):
        if not self._writer.isOpened():
            return False
        else:
            try:
                self._writer.write(im)
                return True
            except:
                return False
