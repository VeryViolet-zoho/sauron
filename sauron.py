import os
import click
import json

from monitor.monitor import Monitor

from config import BASE_DIR


@click.command()
def fmain():
    click.secho('\nSauron v3.1\n\n', fg='cyan')

    monitor = Monitor()

    monitor.run(BASE_DIR)

    click.secho('--> Terminated.', fg='cyan')

    click.secho('--> Done.\n', fg='cyan')


if __name__ == '__main__':
    fmain()
